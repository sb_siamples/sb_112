package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb112Application {

	public static void main(String[] args) {
		SpringApplication.run(Sb112Application.class, args);
	}

}
