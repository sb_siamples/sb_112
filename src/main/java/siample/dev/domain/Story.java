package siample.dev.domain;

import java.util.Date;

public class Story {
	
	private String title;
	private String author;
	private String content;
	private Date posted;
	public Story() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Story(String title, String author, String content, Date posted) {
		super();
		this.title = title;
		this.author = author;
		this.content = content;
		this.posted = posted;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getPosted() {
		return posted;
	}
	public void setPosted(Date posted) {
		this.posted = posted;
	}
	@Override
	public String toString() {
		return "Story [title=" + title + ", author=" + author + ", content=" + content + ", posted=" + posted + "]";
	} 

	
}
